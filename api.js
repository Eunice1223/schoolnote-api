var admin = require("firebase-admin");
const Joi = require('joi');
const express = require('express');
const bodyParser = require('body-parser');
var multer  = require('multer')();
var fs = require('fs'); 
var JSFtp = require("jsftp");
const sql = require('mssql');
const cors = require('cors');

const app = express();
app.use(cors()); 
app.use(express.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
var serviceAccount = require("./schoolNoteKey.json");
const { join } = require("path");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://school-note-b5b4c.firebaseio.com"
})
const port = 1711;
const base = '/api/';
const filesPath = "/htdocs/user-files/"
const filesURL = "http://schoolnote.infinityfreeapp.com/user-files/"
const dbConfigAmazon = {
    server: 'schoolnotev3.cfbntdcyzzy0.us-east-2.rds.amazonaws.com',
    user: 'adminSN', //update me
    password: 'SchoolNote2020',
    database: 'schoolNoteV3',
    port: 1433
};
const dbConfig = { 
    user: 'sa', 
    password: 'Eunice2312', 
    server: 'localhost', 
    database: 'schoolNoteV3',
    port: 1433
};
const notification_options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
  };
async function connectDB() {
    try {
        const pool = new sql.ConnectionPool(dbConfigAmazon);
        await pool.connect();
        return pool;
    }
    catch (err) {
        console.log('Database connection failed!', err);
        return err;
    }
}
function sendNotificationToToken(registrationToken, title, body){
    var message = {
        notification: {
            title: title,
            body: body
        }
    };
    admin.messaging().sendToDevice(registrationToken, message, notification_options)
      .then( response => {
       res.status(200).send("Notification sent successfully")
      })
      .catch( error => {
          console.log(error);
      });
}
function sendNotificationMultipleUsers(tokens, body, title){
    var message = {
        tokens: tokens,
        notification: {
            title: title,
            body: body
        },
        priority: "high"
    };
    admin.messaging().sendMulticast(message, false,function (err, response) {
        if (err) {
        console.log("Something has gone wrong!", err);
        } else {
        console.log("Successfully sent with response: ", response.results);
        res.send("Successfully sent");
        }
    });
}
function uploadFile(file, path){
const Ftp = new JSFtp({
    host: "ftpupload.net",
    port: 21, // defaults to 21
    user: "epiz_26968781", // defaults to "anonymous"
    pass: "t3bw1d06",
    debugMode: true  // defaults to "@anonymous"
});
Ftp.on('jsftp_debug', function(eventType, data) {
    console.log('DEBUG: ', eventType);
    console.log(JSON.stringify(data, null, 2));
});
Ftp.put(file.buffer, path, function(err) {
    if (err) {
        console.log(err);
    }
});
}
app.post(base + 'login', async (req, res) => {

    const schema = {
        email: Joi.string().required(),
        pass: Joi.string().required()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('email', sql.VarChar, req.body.email)
            .input('pass', sql.VarChar, req.body.pass)
            .query("Select * from usuario  where correo=@email AND contraseña=@pass");
        if (data.recordset.length > 0)
            res.status(200).json({success: 1, message: "Ok", data:data.recordset[0]});
        else
            res.status(200).json({success: 0, message: "Datos de login incorrectos" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'registro', async (req, res) => {
    const schema = {
        email: Joi.string().required(),
        pass: Joi.string().required(),
        name: Joi.string().required(),
        lastname: Joi.string().required() 
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('email', sql.VarChar, req.body.email)
            .query("Select * from usuario  where correo=@email");
        if (data.recordset.length > 0)
            res.status(200).json({success: 0, message: "El correo ingresado ya está registrado"});
        else{
            const fullname = req.body.name+" "+req.body.lastname
            await db.request()
            .input('email', sql.VarChar, req.body.email)
            .input('pass', sql.VarChar, req.body.pass)
            .input('name', sql.VarChar, fullname)
            .query("INSERT INTO Usuario (Nombre, Correo, contraseña) VALUES (@name, @email, @pass)");
            const data = await db.request()
            .input('email', sql.VarChar, req.body.email)
            .query("Select * from usuario  where correo=@email");
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message: "Ok", data: data.recordset[0]});
            else
                res.status(200).json({ success: 0, message: "Ocurrió un error al registrar la información" });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'agregarToken', async (req, res) => {

    const schema = {
        idUsuario: Joi.string().required(),
        token: Joi.string().required()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('id', sql.VarChar, req.body.idUsuario)
            .input('token', sql.VarChar, req.body.token)
            .query("UPDATE Usuario SET token=@token  where idUsuario=@id");
        res.status(200).json({success: 1, message: "token agregado"});
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'agregarMateria', async (req, res) => {
        const schema = {
            idUsuario: Joi.string().required(),
            nombre: Joi.string().required(),
            color: Joi.string().required()
        };
        const { error } = Joi.validate(req.body, schema);
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
                const data = await db.request()
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .input('nombre', sql.VarChar, req.body.nombre)
                .input('color', sql.VarChar, req.body.color)
                .query("INSERT INTO Materia (idUsuario, nombreMateria, color) OUTPUT Inserted.idMateria VALUES (@idUsuario, @nombre, @color)");
                if (data.recordset.length > 0)
                    res.status(200).json({success: 1, message: "Ok", data: data.recordset[0]});
                else
                    res.status(200).json({ success: 0, message: "Ocurrió un error al registrar la información" });
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    });
app.post(base + 'editarMateria', async (req, res) => {
    const schema = {
        idMateria: Joi.string().required(),
        nombre: Joi.string(),
        color: Joi.string()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
            if(req.body.nombre!= undefined){
            const data = await db.request()
            .input('idMateria', sql.Int, req.body.idMateria)
            .input('nombre', sql.VarChar, req.body.nombre)
            .query("UPDATE Materia SET nombreMateria = @nombre WHERE idMateria = @idMateria");
            }
            if(req.body.color != undefined){
                const data = await db.request()
                .input('idMateria', sql.Int, req.body.idMateria)
                .input('color', sql.VarChar, req.body.color)
                .query("UPDATE Materia SET color = @color WHERE idMateria = @idMateria");
                }
            res.status(200).json({success: 1, message: "se modifico la informacion"});
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'eliminarMateria', async (req, res) => {
    const schema = {
        idMateria: Joi.string().required()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
            const data = await db.request()
            .input('idMateria', sql.Int, req.body.idMateria)
            .query("DELETE FROM Materia WHERE idMateria = @idMateria");
            
            res.status(200).json({success: 1, message: "se elimino el registro"});
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'obtenerInformacionMateria', async (req, res) => {
    const schema = {
        idMateria: Joi.string().required()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
            const data = await db.request()
            .input('idMateria', sql.Int, req.body.idMateria)
            .query("SELECT m.idMateria, m.nombreMateria, m.color, gru.idGrupo, gr.codigo, gr.horaInicio, gr.horaFin, gr.idUsuarioAdmin as admin From Materia m left join (Grupo_Usuario gru inner join Grupo gr on gru.idGrupo = gr.idGrupo) on m.idMateria = gru.idMateria WHERE m.idMateria = @idMateria");
            res.status(200).json({success: 1, message: "OK", data: data.recordset[0]});
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'obtenerListaMaterias', async (req, res) => {
    const schema = {
        idUsuario: Joi.string().required()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
            const data = await db.request()
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .query("SELECT * FROM Materia WHERE idUsuario = @idUsuario");
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message: "Ok", data: data.recordset});
            else
                res.status(200).json({ success: 0, message: "Ocurrió un error al obtener la información" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'obtenerMateriasHorario', async (req, res) => {
    const schema = {
        idUsuario: Joi.string().required(),
        dia: Joi.number().required(),
        hora: Joi.string().required()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
            let query = "SELECT * FROM Materia WHERE idUsuario = @idUsuario "
            const currentClass = await db.request()
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .input('dia', sql.Int, req.body.dia)
            .input('hora', sql.VarChar, req.body.hora)
            .query("SELECT m.* FROM Materia m INNER JOIN Horario h on m.idMateria = h.idMateria WHERE h.dia = @dia AND h.horaInicio <= @hora AND h.horaFin >= @hora AND m.idUsuario = @idUsuario");
            let idMateria = 0;
            if (currentClass.recordset.length > 0){
                query = query + "AND idMateria != @idMateria";
                idMateria = currentClass.recordset[0].idMateria;
            }
            const data = await db.request()
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .input('idMateria', sql.Int, idMateria)
            .query(query);
            if (currentClass.recordset.length > 0){
                data.recordset.unshift(currentClass.recordset[0]);
            }
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message: "Ok", data: data.recordset});
            else
                res.status(200).json({ success: 0, message: "Ocurrió un error al obtener la información" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'agregarHorario', async (req, res) => {
        const schema = {
            idUsuario: Joi.string().required(),
            idMateria: Joi.string().required(),
            dia: Joi.string().required(),
            rangos: Joi.string().required() 
        };
        const { error } = Joi.validate(req.body, schema);
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
                const rangos = req.body.rangos.split(",");
                for (rango of rangos){
                    horas = rango.split("-");
                    const data = await db.request()
                    .input('hInicio', sql.VarChar, horas[0])
                    .input('hFin', sql.VarChar, horas[1])
                    .input('idMateria', sql.Int, req.body.idMateria)
                    .input('dia', sql.Int, req.body.dia)
                    .query("INSERT INTO Horario (horaInicio, horaFin, idMateria, dia) OUTPUT Inserted.idHorario VALUES ( @hInicio, @hFin, @idMateria, @dia)");
                    /*await db.request()
                    .input('idHorario', sql.Int, data.recordset[0].idHorario)
                    .input('idUsuario', sql.Int, req.body.idUsuario)
                    .query("INSERT INTO Horario_Usuario (idHorario, idUsuario) OUTPUT Inserted.idHorario VALUES ( @idHorario, @idUsuario)");*/
                }
                res.status(200).json({success: 1, message: "Informacion guardada"});
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{ 
            db.close();
        }
    });
    app.post(base + 'editarHorario', async (req, res) => {
        const schema = {
            idMateria: Joi.string().required(),
            dia: Joi.string().required(),
            rangos: Joi.string()
        };
        const { error } = Joi.validate(req.body, schema);
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            const data = await db.request()
            .input('idMateria', sql.Int, req.body.idMateria)
            .input('dia', sql.VarChar, req.body.dia)
            .query("DELETE FROM Horario WHERE idMateria = @idMateria AND dia = @dia")
            try{
                if( req.body.rangos != null && req.body.rangos != undefined && req.body.rangos != "0"){
                    const rangos = req.body.rangos.split(",");
                    for (rango of rangos){
                        horas = rango.split("-");
                        const data = await db.request()
                        .input('hInicio', sql.VarChar, horas[0])
                        .input('hFin', sql.VarChar, horas[1])
                        .input('idMateria', sql.Int, req.body.idMateria)
                        .input('dia', sql.Int, req.body.dia)
                        .query("INSERT INTO Horario (horaInicio, horaFin, idMateria, dia) OUTPUT Inserted.idHorario VALUES ( @hInicio, @hFin, @idMateria, @dia)");
                        /*await db.request()
                        .input('idHorario', sql.Int, data.recordset[0].idHorario)
                        .input('idUsuario', sql.Int, req.body.idUsuario)
                        .query("INSERT INTO Horario_Usuario (idHorario, idUsuario) OUTPUT Inserted.idHorario VALUES ( @idHorario, @idUsuario)");*/
                    }
                }
                    res.status(200).json({success: 1,  message: "Informacion guardada"});
            }catch (error) {
                console.log(error);
                res.status(500).send("Error de conexión");
            }
              
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{ 
            db.close();
        }
    });
app.post(base + 'obtenerHorario', async(req,res)=>{

        const schema = Joi.object({ 
            idUsuario: Joi.string().required()
        });
        console.log(req.body)
        const { error } = schema.validate(req.body);
        console.log(error)
        if (error)
            return res.status(400).json({ success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            const data = await db.request()
                .input('idUsuario', sql.VarChar, req.body.idUsuario)
                .query(`sp_c_horario @idUsuario=@idUsuario`);
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message: "Ok", data: data.recordset});
            else
                res.status(200).json({ success: 0, message: "No se encontró horario para el usuario." });
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    
    });
    
app.post(base + 'obtenerMultimediaM', async(req,res)=>{
    
        const schema = Joi.object({ 
            idUsuario: Joi.string(),
            idMateria: Joi.string(),
            idGrupo: Joi.string()
        });
        console.log(req.body)
        const { error } = schema.validate(req.body);
        console.log(error)
        if (error)
            return res.status(400).json({ success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            const data = await db.request()
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .input('idMateria', sql.Int, req.body.idMateria)
                .input('idGrupo', sql.Int, req.body.idGrupo)
                .query(`sp_c_Multimedia @idUsuario=@idUsuario, @idMateria=@idMateria, @idGrupo=@idGrupo`);
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message: "Ok", data: data.recordset});
            else
                res.status(200).json({ success: 0, message: "No se encontró multimedia para el usuario." });
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    
    });
    app.post(base + 'obtenerPendientes', async(req,res)=>{
        const schema = Joi.object({ 
            idUsuario: Joi.string().required(),
            idMateria: Joi.string()
        });
        console.log(req.body)
        const { error } = schema.validate(req.body);
        console.log(error)
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            let query = "SELECT p.* FROM Pendiente p INNER JOIN Materia m on p.idMateria = m.idMateria where m.idUsuario=@idUsuario "
            if(req.body.idMateria != undefined){
                query += "AND p.idMateria = @idMateria "
            }
            query += "ORDER BY fechaEntrega"
            const data = await db.request()
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .input('idMateria', sql.Int, req.body.idMateria)
                .query(query);
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message: "Ok", data: data.recordset});
            else
                res.status(200).json({success: 0, message: "No se encontraron pendientes para el usuario." });
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    
    });    
app.post(base + 'agregarMultimedia', multer.single('archivo'), async(req,res)=>{
        const schema = Joi.object({ 
            idUsuario: Joi.string(),
            fecha: Joi.date(),
            idMateria: Joi.string(),
            idGrupo: Joi.string(),
            etiqueta: Joi.string(),
            nombre: Joi.string()
        });
        console.log(req.body)
        const { error } = schema.validate(req.body);
        console.log(error)
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            const fileRecievedFromClient = req.file;
            const fileName = req.body.idUsuario + Date.now() + req.body.nombre;
            uploadFile(fileRecievedFromClient, filesPath + fileName);
            const data = await db.request()
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .input('archivo', sql.VarChar, filesURL + fileName)
                .input('fecha', sql.Date, req.body.fecha)
                .input('idMateria', sql.Int, req.body.idMateria)
                .input('idGrupo', sql.Int, req.body.idGrupo)
                .input('etiqueta', sql.VarChar, req.body.etiqueta)
                .input('nombre', sql.VarChar, req.body.nombre)
                .query(`sp_i_Multimedia @idUsuario=@idUsuario, @idMateria=@idMateria, @archivo = @archivo, @fecha = @fecha, @idGrupo = @idGrupo, @etiqueta = @etiqueta, @nombre = @nombre `);
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message: "Ok", data: data.recordset[0]});
            else
                res.status(200).json({success: 0, message: "No se agregó la multimedia." });
            if(req.body.idGrupo != undefined){
                const userData = await db.request()
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .query('SELECT * FROM usuario WHERE idUsuario = @idUsuario');
                let message = userData.recordset[0].nombre + " ha compartido un archivo en el grupo";
                await db.request()
                .input('idGrupo', sql.Int, req.body.idGrupo)
                .input('mensaje', sql.VarChar, message)
                .query('INSERT INTO Chat (idGrupo, mensaje) VALUES (@idGrupo, @mensaje)');
            }
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    
    });
   
app.post(base + 'obtenerMultimediaReciente', async(req,res)=>{
    
        const schema = Joi.object({ 
            idUsuario: Joi.string()
        });
        
        console.log(req.body)
        const { error } = schema.validate(req.body);
        console.log(error)
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            const data = await db.request()
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .query(`sp_c_MultimediaRec @idUsuario=@idUsuario`);
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message: "Ok", data: data.recordset});
            else
                res.status(200).json({success: 0, message: "No se encontró multimedia para el usuario." });
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    
    });

app.post(base + 'obtenerAnuncios', async(req,res)=>{
    
        const schema = Joi.object({ 
            idUsuario: Joi.string(),
            idMateria: Joi.string()
        });
        
        console.log(req.body)
        const { error } = schema.validate(req.body);
        console.log(error)
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            const data = await db.request()
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .query(`sp_c_Publicidad @idUsuario=@idUsuario`);
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message: "Ok", data: data.recordset});
            else
                res.status(200).json({success: 0, message: "No se encontraron anuncios para el usuario." });
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    
    });

app.post(base + 'eliminarMultimedia', async(req,res)=>{
    
    const schema = Joi.object({ 
        idUsuario: Joi.string(),
        idMultimedia: Joi.string(),
        idGrupo: Joi.string()
    });
    
    console.log(req.body)
    const { error } = schema.validate(req.body);
    console.log(error)
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .input('idMultimedia', sql.Int, req.body.idMultimedia)
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .query(`sp_d_Multimedia @idUsuario=@idUsuario, @idMultimedia=@idMultimedia, @idGrupo = @idGrupo`);
        if (data.recordset.length > 0)
            res.status(200).json({success: 1, message: "Ok", data: data.recordset[0]});
        else
            res.status(200).json({success: 0, message: "No se encontraron pendientes para el usuario." });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }

});

app.post(base + 'eliminarPendiente', async(req,res)=>{
    
    const schema = Joi.object({ 
        idUsuario: Joi.string(),
        idPendiente: Joi.string(),
        idGrupo: Joi.string()
    });
    
    console.log(req.body)
    const { error } = schema.validate(req.body);
    console.log(error)
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .input('idPendiente', sql.Int, req.body.idPendiente)
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .query(`sp_d_Pendientes @idUsuario=@idUsuario, @idGrupo=@idGrupo, @idPendiente = @idPendiente`);
        if (data.recordset.length > 0)
            res.status(200).json({success: 1, message: "Ok", data: data.recordset[0]});
        else
            res.status(200).json({success: 0, message: "No se encontraron pendientes para el usuario." });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }

});

app.delete(base + 'eliminarUsuarios', async(req,res)=>{
    
    const schema = Joi.object({ 
        idUsuario: Joi.string().required(),
        idGrupo: Joi.string().required()
    });
    
    console.log(req.body)
    const { error } = schema.validate(req.body);
    console.log(error)
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .query(`sp_d_Usuarios @idUsuario=@idUsuario, @idGrupo=@idGrupo`);
        if (data.recordset.length > 0)
            res.status(200).json({success: 1, message: "Ok", data: data.recordset[0]});
        else
            res.status(200).json({success: 0, message: "No se encontraron pendientes para el usuario." });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }

});

app.post(base + 'agregarEtiqueta', async (req, res) => {
    const schema = {
        idMultimedia: Joi.string().required(),
        etiqueta: Joi.string().required()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
            const data = await db.request()
            .input('idMultimedia', sql.VarChar, req.body.idMultimedia)
            .input('etiqueta', sql.VarChar, req.body.etiqueta)
            .query("sp_i_Etiqueta @idMultimedia = @idMultimedia, @etiqueta = @etiqueta");
            if (data.recordset.length > 0)
                res.status(200).json({success: 1, message:"Ok", data: data.recordset[0]});
            else
                res.status(200).json({ success: 0, message: "Ocurrió un error al registrar la información" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'compartirMultimedia', async (req, res) => {
    const schema = {
        idGrupo: Joi.string().required(),
        idUsuario: Joi.string().required(),
        idsMultimedia: Joi.string().required()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
            let query = "INSERT INTO Multimedia_Grupo (idGrupo, idMultimedia) VALUES (@idGrupo, @idMultimedia)"
            let ids = req.body.idsMultimedia.split(",");
            for(id of ids){
                let idM = id.replace(/\s/g,'');
                await db.request()
                .input('idMultimedia', sql.Int, idM)
                .input('idGrupo', sql.Int, req.body.idGrupo)
                .query(query);
            }
            res.status(200).json({success: 1, message:"Se compartieron los archivos al grupo"});
            const userData = await db.request()
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .query('SELECT * FROM usuario WHERE idUsuario = @idUsuario');
            let message = userData.recordset[0].nombre + " ha compartido un archivo en el grupo";
            await db.request()
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .input('mensaje', sql.VarChar, message)
            .query('INSERT INTO Chat (idGrupo, mensaje) VALUES (@idGrupo, @mensaje)');
} catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
app.post(base + 'editarPendiente', async (req, res) => {
    const schema = {
        idMateria: Joi.string().required(),
        descripcion: Joi.string(),
        titulo: Joi.string(),
        idPendiente: Joi.string(),
        fecha: Joi.date()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
            const data = await db.request()
            .input('idMateria', sql.Int, req.body.idMateria)
            .input('descripcion', sql.VarChar, req.body.descripcion)
            .input('titulo', sql.VarChar, req.body.titulo)
            .input('fecha', sql.DateTime, req.body.fecha)
            .input('idPendiente', sql.Int, req.body.idPendiente)
            .query("sp_u_Pendiente @idPendiente = @idPendiente,  @idMateria = @idMateria, @descripcion = @descripcion, @fecha = @fecha, @titulo = @titulo");
            if (data.recordset.length > 0)
                res.status(200).json({success: 1,  message:"Ok", data: data.recordset[0]});
            else
                res.status(200).json({ success: 0, message: "Ocurrió un error al registrar la información" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});    
app.post(base + 'agregarPendiente', async(req,res)=>{
    
        const schema = Joi.object({ 
            idUsuario: Joi.string(),
            fecha: Joi.date(),
            idMateria: Joi.string(),
            idGrupo: Joi.string(),
            descripcion: Joi.string(),
            titulo: Joi.string()
        });
        
        console.log(req.body)
        const { error } = schema.validate(req.body);
        console.log(error)
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            const data = await db.request()
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .input('fecha', sql.Date, req.body.fecha)
                .input('idMateria', sql.Int, req.body.idMateria)
                .input('idGrupo', sql.Int, req.body.idGrupo)
                .input('descripcion', sql.VarChar, req.body.descripcion)
                .input('titulo', sql.VarChar, req.body.titulo)
                .query(`sp_i_Pendiente @idUsuario=@idUsuario, @idMateria=@idMateria, @fecha = @fecha, @idGrupo = @idGrupo, @descripcion = @descripcion, @titulo = @titulo `);
            if (data.recordset.length > 0)
                res.status(200).json({success: 1,  message:"Ok", data: data.recordset[0]});
            else
                res.status(200).json({success: 0, message: "No se agregó el pendiente." });
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    
    });
    
app.post(base + 'crearGrupo', async(req,res)=>{
    
        const schema = Joi.object({ 
            idUsuario: Joi.string(),
            idMateria: Joi.string(),
            nombre: Joi.string()
        });
        
        console.log(req.body)
        const { error } = schema.validate(req.body);
        console.log(error)
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            const data = await db.request()
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .input('idMateria', sql.Int, req.body.idMateria)
                .input('nombre', sql.VarChar, req.body.nombre)
                .query('sp_i_Grupo @idUsuario=@idUsuario, @idMateria=@idMateria, @nombre = @nombre');
                await db.request()
                .input('idGrupo', sql.Int, data.recordset[0].idGrupo)
                .input('idMateria', sql.Int, req.body.idMateria)
                .input('idUsuario', sql.Int, req.body.idUsuario)
                .query('INSERT INTO Grupo_Usuario (idGrupo, idUsuario, idMateria) VALUES (@idGrupo, @idUsuario, @idMateria)');
            if (data.recordset.length > 0)
                res.status(200).json({success: 1,  message:"Ok", data: data.recordset[0]});
            else
                res.status(200).json({success: 0, message: "No se creó el grupo." });
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    
    });
app.post(base + 'editarGrupo', async(req,res)=>{
    
        const schema = Joi.object({ 
            idGrupo: Joi.string().required(),
            nombre: Joi.string(),
            horaInicio: Joi.string(),
            horaFin: Joi.string()
        });
        
        console.log(req.body)
        const { error } = schema.validate(req.body);
        console.log(error)
        if (error)
            return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
        const db = await connectDB();
        try {
            let query = "UPDATE Grupo SET";
            if(req.body.nombre != undefined){
                query += " nombre = @nombre ";
                if(req.body.horaInicio != undefined)
                query += ", ";
            }
            if(req.body.horaInicio != undefined)
                query += " horaInicio = @horaInicio, ";
            if(req.body.horaFin != undefined)
                query += " horaFin = @horaFin ";
            query += "WHERE idGrupo = @idGrupo";
            const data = await db.request()
                .input('horaInicio', sql.VarChar, req.body.horaInicio)
                .input('horaFin', sql.VarChar, req.body.horaFin)
                .input('nombre', sql.VarChar, req.body.nombre)
                .input('idGrupo', sql.Int, req.body.idGrupo)
                .query(query);
            res.status(200).json({success: 1,  message:"Se actualizo la información"});
        } catch (error) {
            console.log(error);
            res.status(500).send("Error de conexión");
        }
        finally{
            db.close();
        }
    
    });
app.post(base + 'obtenerUsuariosGrupo', async(req,res)=>{

    const schema = Joi.object({ 
        idGrupo: Joi.string().required()
    });
    
    console.log(req.body)
    const { error } = schema.validate(req.body);
    console.log(error)
    if (error)
        return res.status(400).json({ success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .query('SELECT u.idUsuario, Nombre, foto FROM Grupo_Usuario g INNER JOIN Usuario  u on g.idUsuario = u.idUsuario WHERE g.idGrupo = @idGrupo');
        if (data.recordset.length > 0)
            res.status(200).json({success: 1, message: "Ok", data: data.recordset});
        else
            res.status(200).json({ success: 0, message: "No se pudieron obtener los usuarios" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }

});
app.post(base + 'cambiarAdminGrupo', async(req,res)=>{

    const schema = Joi.object({ 
        idGrupo: Joi.string().required(),
        idUsuario: Joi.string().required()
    });
    
    console.log(req.body)
    const { error } = schema.validate(req.body);
    console.log(error)
    if (error)
        return res.status(400).json({ success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .query('UPDATE Grupo SET idUsuarioAdmin=@idUsuario WHERE idGrupo = @idGrupo');
            res.status(200).json({success: 1, message: "Se cambio el administrador del grupo"});
        } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }

});
app.post(base + 'unirseGrupo', async(req,res)=>{
    
    const schema = Joi.object({ 
        idUsuario: Joi.string().required(),
        idMateria: Joi.string().required(),
        codigo: Joi.string().required()
    });
    
    console.log(req.body)
    const { error } = schema.validate(req.body);
    console.log(error)
    if (error)
        return res.status(400).json({ success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('codigo', sql.Int, req.body.codigo)
            .query('SELECT * FROM Grupo WHERE codigo = @codigo');
        if (data.recordset.length > 0){
            await db.request()
            .input('idGrupo', sql.Int, data.recordset[0].idGrupo)
            .input('idMateria', sql.Int, req.body.idMateria)
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .query('INSERT INTO Grupo_Usuario (idGrupo, idUsuario, idMateria) VALUES (@idGrupo, @idUsuario, @idMateria)');
            res.status(200).json({success: 1, message: "se ha unido al grupo"});
        }
        else
            res.status(200).json({ success: 0, message: "No existe un grupo con ese código" });
        const userData = await db.request()
        .input('idUsuario', sql.Int, req.body.idUsuario)
        .query('SELECT * FROM usuario WHERE idUsuario = @idUsuario');
        let message = userData.recordset[0].nombre + " se ha unido al grupo";
        await db.request()
        .input('idGrupo', sql.Int, data.recordset[0].idGrupo)
        .input('mensaje', sql.VarChar, message)
        .query('INSERT INTO Chat (idGrupo, mensaje) VALUES (@idGrupo, @mensaje)');
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }

});
app.post(base + 'enviarMensaje', async(req,res)=>{

    const schema = Joi.object({ 
        idGrupo: Joi.string().required(),
        idUsuario: Joi.string().required(),
        mensaje: Joi.string().required()
    });
    
    console.log(req.body)
    const { error } = schema.validate(req.body);
    console.log(error)
    if (error)
        return res.status(400).json({ success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const data = await db.request()
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .input('mensaje', sql.VarChar, req.body.mensaje)
            .query('INSERT INTO Chat (idGrupo, idUsuario, mensaje) OUTPUT Inserted.stamp VALUES (@idGrupo, @idUsuario, @mensaje)');
        if (data.recordset.length > 0){
            res.status(200).json({success: 1,  message: "Ok", data: data.recordset[0]});
            const info = await db.request()
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .query('SELECT us.nombre, g.nombre as nombreGrupo from usuario us INNER JOIN (Grupo_Usuario u INNER JOIN Grupo g on u.idGrupo = g.idGrupo) on us.idUsuario = u.idUsuario WHERE us.idUsuario = @idUsuario and g.idGrupo = @idGrupo');
            const users = await db.request()
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .query('SELECT u.token from Grupo_Usuario g INNER JOIN usuario u on g.idUsuario = u.idUsuario WHERE idGrupo = @idGrupo AND g.idUsuario != @idUsuario AND u.token IS NOT NULL');
            if (users.recordset.length > 0){
                let tokens= users.recordset.map(function(a) {return a.token;});
                let nombreUsuario = info.recordset[0].nombre;
                let nombreGrupo = info.recordset[0].nombreGrupo;
                let titulo = nombreUsuario + " @ " + nombreGrupo;
                sendNotificationMultipleUsers(tokens,req.body.mensaje, titulo)
            }
        }
        else
            res.status(200).json({ success: 0, message: "No se pudo envíar el mensaje" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }

});
app.post(base + 'subirFotoPerfil' , multer.single('archivo'), async(req , res) => {
    const schema = Joi.object({ 
        idUsuario: Joi.string()
    });
    console.log(req.body)
    const { error } = schema.validate(req.body);
    console.log(error)
    if (error)
        return res.status(400).json({ success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const fileRecievedFromClient = req.file;
        const fileName = req.body.idUsuario + Date.now()+".jpeg";
        uploadFile(fileRecievedFromClient, filesPath + fileName);
        let query = 'UPDATE usuario SET foto=@foto OUTPUT  inserted.idUsuario, inserted.nombre, inserted.correo, inserted.contraseña, inserted.foto, inserted.token WHERE idUsuario=@idUsuario';
        const data = await db.request()
            .input('idUsuario', sql.Int, req.body.idUsuario)
            .input('foto', sql.VarChar, filesURL + fileName)
            .query(query);
        if (data.recordset.length > 0)
            res.status(200).json({success: 1,  message: "Se cambió la foto de perfil", data:data.recordset[0]});
        else
            res.status(200).json({ success: 0, message: "No se pudo subir el archivo" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
})
app.post(base + 'obtenerMensajes', async(req,res)=>{

    const schema = Joi.object({ 
        idGrupo: Joi.string().required(),
        idChat: Joi.string()
    });
    
    console.log(req.body)
    const { error } = schema.validate(req.body);
    console.log(error)
    if (error)
        return res.status(400).json({ success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        let query = 'SELECT TOP (40) c.*, u.foto, u.nombre FROM Chat c LEFT JOIN usuario u on c.idUsuario = u.idUsuario WHERE idGrupo = @idGrupo ORDER BY c.idChat DESC ';
        if (req.body.idChat != undefined){
            query = query + ' AND c.idChat < @idChat ';
        }
        const data = await db.request()
            .input('idGrupo', sql.Int, req.body.idGrupo)
            .input('idChat', sql.Int, req.body.idChat)
            .query(query);
        if (data.recordset.length > 0)
            res.status(200).json({success: 1,  message: "Ok", data: data.recordset});
        else
            res.status(200).json({ success: 0, message: "No se pudieron obtener los mensajes" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }

});
app.post(base + 'editarPerfil', async (req, res) => {
    const schema = {
        idUsuario: Joi.string().required(),
        pass: Joi.string(),
        name: Joi.string(),
        lastname: Joi.string(),
        foto: Joi.string()
    };
    const { error } = Joi.validate(req.body, schema);
    if (error)
        return res.status(400).json({success: 0, message: "No se estan enviando los parámetros necesarios" });
    const db = await connectDB();
    try {
        const fullname = req.body.name+" "+req.body.lastname
        let query = 'UPDATE usuario SET ';
        query = query + 'contraseña = '+ (req.body.pass != undefined ? "@pass, " : "contraseña, ");
        query = query + 'Nombre = '+ (req.body.name != undefined ? "@name, " : "Nombre, ");
        query = query + 'foto = '+ (req.body.foto != undefined ? "@foto " : "foto");
        query = query + " OUTPUT  inserted.idUsuario, inserted.nombre, inserted.correo, inserted.contraseña, inserted.foto, inserted.token WHERE idUsuario = @idUsuario";
        const data =await db.request()
        .input('idUsuario', sql.VarChar, req.body.idUsuario)
        .input('pass', sql.VarChar, req.body.pass)
        .input('name', sql.VarChar, fullname)
        .input('foto', sql.VarChar, req.body.foto)
        .query(query);
            res.status(200).json({success: 1, message: "Información actualizada", data: data.recordset[0]});
    } catch (error) {
        console.log(error);
        res.status(500).send("Error de conexión");
    }
    finally{
        db.close();
    }
});
console.log("Listening on port "+port);
app.listen(port);