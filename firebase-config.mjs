var admin = require("firebase-admin");

var serviceAccount = require("./schoolNoteKey.json");


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://school-note-b5b4c.firebaseio.com"
})

module.exports.admin = admin